# nsq-nodejs

This example runs `nsqd`, `nsqlookupd` and `nsqadmin`.  
Some small NodeJS applications interacting with NSQ to produce and consume messages.  
It's an example to use NSQ as communication layer in a microservice architecture.  

# Run it
Run the complete example and see the console output.

    $ docker-compose up --build
    $ docker-compose up --build --scale writer=4
    $ docker-compose up --build --scale writer=4 --scale reader=2
    $ docker-compose up --build --scale writer=4 --scale reader=2 --scale processor=3

The HTTP API from NSQ can be browsed under this URL.

    http://localhost:4151/

Publish a message with `curl`.  

    curl -d '{"data": "JSON"}' http://127.0.0.1:4151/pub?topic=topic

You can browse `nsqadmin` under this URL.

    http://localhost:4171/

# Play around
Run only the communication layer.

    $ docker-compose up nsqlookup nsq nsqadmin

You can then start the node applications local.

    $ cd app
    $ npm run reader
    $ npm run writer
    $ npm run processor

## Feedback
Star this repo if you found it useful. Use the github issue tracker to give feedback on this repo.
