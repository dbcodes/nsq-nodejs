const nsq = require('nsqjs');

const server = process.env.server || 'localhost';
const port = process.env.port || 4150;
const lookupServer = process.env.lookup || 'localhost:4160';
const topic = process.env.topic || 'topic';
const channel = process.env.channel || 'channel';

const reader = new nsq.Reader(topic, channel, {
  nsqdTCPAddresses: [`${server}:${port}`],
  lookupdHTTPAddresses: [`${lookupServer}`],
});

const logger = new nsq.Reader('log', channel, {
  nsqdTCPAddresses: [`${server}:${port}`],
  lookupdHTTPAddresses: [`${lookupServer}`],
});
logger.on('message', (msg) => {
  console.log(`Log message ${msg.id} ${msg.body.toString()}`);
  msg.finish();
});

const writer = new nsq.Writer(server, port, {
  clientId: 'writer',
});

reader.connect();
logger.connect();
writer.connect();

writer.on('ready', () => {
  console.log(`Ready to publish!`);  

  reader.on('message', (msg) => {
    console.log(`Received message ${msg.id}`);
    let msgData;
    try {
      msgData = msg.json();
      msgData.proceed = true;
      console.log(msgData);
    } catch (err) {
      console.log('JSON parse error', msg.body.toString());
    }
    writer.publish('log', JSON.stringify(msgData),  (err) => {
      if (err) { return console.error(err.message); };
      console.log('Message sent successfully again');
      msg.finish();
    });    
  });

});