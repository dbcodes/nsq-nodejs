const nsq = require('nsqjs');

const nsqServer = process.env.server || 'localhost:4150';
const lookupServer = process.env.lookup || 'localhost:4160';
const topic = process.env.topic || 'topic';
const channel = process.env.channel || 'channel';

let msgCounter = 0;

const reader = new nsq.Reader(topic, channel, {
  nsqdTCPAddresses: [`${nsqServer}`],
  lookupdHTTPAddresses: [`${lookupServer}`],
});

reader.connect();

reader.on('nsqd_connected', (host, port) => {
  console.log(`Connected to nsqd ${host} on ${port}.`);
});

reader.on('nsqd_closed', (host, port) => {
  console.log(`Closed connection to nsqd ${host} on ${port}.`);
});

reader.on('discard', (msg) => {
  console.log(`Discard message ${msg.id}.`);
});

reader.on('message', (msg) => {
  msgCounter += 1;  
  try {
    console.log(msg.json());
  } catch (err) {
    console.log('JSON parse error', msg.body.toString());
  }

  msg.finish();
  console.log(`Received message ${msg.id} ${msg.timestamp} it is the ${msgCounter} message.`);  
  console.log(msg.attempts, `message time until timeout: ${msg.timeUntilTimeout()} has been responded: ${msg.hasResponded}`);

  // after ten messages the reader take a break
  if (msgCounter >= 10) {
    reader.pause();
    console.log('Reader take a break!');
    setTimeout(() => {
      console.log('Reader comes back!');
      reader.unpause();
      msgCounter = 0;      
    }, 5000);        
  }
});