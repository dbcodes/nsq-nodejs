const nsq = require('nsqjs');

const server = process.env.server || 'localhost';
const port = process.env.port || 4150;
const topic = process.env.topic || 'topic';
const interval = process.env.interval || 1000;

const writer = new nsq.Writer(server, port, {
  clientId: 'writer',
});

writer.connect();

const msgs = [
  { content: 'Service available' },
  { content: 'Service ready' },
  { content: 'Service connect' }
];

writer.on('ready', () => {
  setInterval(() => {
    let msg = msgs[Math.floor(Math.random()*msgs.length)];  
    writer.publish(topic, msg,  (err) => {
      if (err) { return console.error(err.message); };
      console.log('Message sent successfully');
    });
  }, interval);
});
